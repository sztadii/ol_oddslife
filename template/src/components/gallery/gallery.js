var gallery;

gallery = new function () {

  //catch DOM
  var $el;
  var $box;
  var $arrows;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $(".gallery");
    $box = $el.find(".gallery__box");
    $arrows = $el.find(".gallery__arrows");

    if ($box.length) {
      $box.imagesLoaded({background: true}).always(function () {
        $box.lightGallery({
          mode: 'lg-slide'
        });

        $box.each(function () {
          $(this).slick({
            infinite: true,
            dots: false,
            arrows: true,
            appendArrows: $arrows,
            nextArrow: '<div class="gallery__arrowBox -next"><img class="gallery__arrow" src="resources/img/gallery__arrow-next.svg" alt="arrow"></div>',
            prevArrow: '<div class="gallery__arrowBox -prev"><img class="gallery__arrow" src="resources/img/gallery__arrow-prev.svg" alt="arrow"></div>',
            slidesToShow: 3,
            slidesToScroll: 1,
            fade: false,
            adaptiveHeight: true,
            speed: 1000,
            autoplay: true,
            autoplaySpeed: 3000,
            draggable: false,
            pauseOnHover: true,
            pauseOnFocus: false,
            responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1
                }
              },
              {
                breakpoint: 700,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]
          });
        });
      });
    }
  };
};